from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import string


class YTHandler(ContentHandler):

    def __init__(self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.id = ""
        self.link = ""
        self.image = ""
        self.published = ""
        self.description = ""
        self.name = ""
        self.url = ""
        self.videos = []

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'media:thumbnail':
                self.image = attrs.get('url')
            elif name == 'published':
                self.inContent = True
            elif name == 'media:description':
                self.inContent = True
            elif name == 'name':
                self.inContent = True
            elif name == 'uri':
                self.inContent = True

    def endElement(self, name):
        global videos

        if name == 'entry':
            self.inEntry = False
            self.videos.append({'link': self.link,
                                'title': self.title,
                                'id': self.id,
                                'image': self.image,
                                'published': self.published,
                                'description': self.description,
                                'name': self.name,
                                'url': self.url})
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.id = self.content
                self.content = ""
                self.inContent = False
            elif name == 'published':
                self.published = self.content
                self.content = ""
                self.inContent = False
            elif name == 'media:description':
                self.description = self.content
                self.content = ""
                self.inContent = False
            elif name == 'name':
                self.name = self.content
                self.content = ""
                self.inContent = False
            elif name == 'uri':
                self.url = self.content
                self.content = ""
                self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars


class YTChannel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos(self):
        return self.handler.videos

