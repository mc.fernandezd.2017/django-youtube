from django.http import HttpResponse
from django.middleware.csrf import get_token
from . import data

# Create your views here.


def construir_html(name, list, action, token):
    html = ""
    for video in list:
        html = html + data.VIDEO.format(link=video['link'], title=video['title'], id=video['id'], name=name,
                                   action=action, token=token, image=video['image'], published=video['published'],
                                   description=video['description'], url=video['url'])
    return html


def mover_video(from_list, to_list, id):
    found = None
    for i, video in enumerate(from_list):
        if video['id'] == id:
            found = from_list.pop(i)
    if found:
        to_list.append(found)


def videos(request, id):
    encontrado = None
    if request.method == 'GET':
        for i, video in enumerate(data.selected):
            if video['id'] == id:
                encontrado = video
        if encontrado != None:
            htmlBody = data.INFORMACION.format(video=encontrado)
            return HttpResponse(htmlBody)
    htmlBody = data.ERROR.format(id=id)
    return HttpResponse(htmlBody)


def index(request):
    if request.method == 'POST':
        if 'id' in request.POST:
            if request.POST.get('select'):
                mover_video(from_list=data.selectable, to_list=data.selected, id=request.POST['id'])
            elif request.POST.get('deselect'):
                mover_video(from_list=data.selected, to_list=data.selectable, id=request.POST['id'])
    csrf_token = get_token(request)
    selected = construir_html(name='deselect', list=data.selected, action='Deseleccionar', token=csrf_token)
    selectable = construir_html(name='select', list=data.selectable, action='Seleccionar', token=csrf_token)
    htmlBody = data.PAGE.format(selected=selected, selectable=selectable)
    return HttpResponse(htmlBody)

