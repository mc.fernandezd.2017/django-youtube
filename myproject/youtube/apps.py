from django.apps import AppConfig
from . import data
from .ytchannel import YTChannel
from urllib.request import urlopen


class YoutubeConfig(AppConfig):
    #default_auto_field = "django.db.models.BigAutoField"
    name = "youtube"

    def ready(self):
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urlopen(url)
        canal = YTChannel(xmlStream)
        data.selectable = canal.videos()

